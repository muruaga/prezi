var appData =  {
  id: 'es-6',
  nivel: '6º Educación Primaria',
  subject : [
    {
      id: 'es-6cn',
      title: '6.º EP Ciencias de la Naturaleza Madrid',
      thumb: 'images/natu.jpg',
      unit: [
        {
          id: 'es-6cn-01',
          number: '1',
          title: 'El cuarpo humano y la relación',
          thumb: 'natu.jpg',
          block: [
            {
              id: 'es-6cn-u01-p01',
              type: 'presentation', //presentation/epigraph / subgraph
              title: 'Presentación',
              subblock: [
                {
                  id: 'es-6cn-u01-p01-se01',
                  title: 'Presentación',
                  type: 'subgraph',
                  html: ''
                },
                {
                  id: 'es-6cn-u01-p01-se02',
                  title: 'Robots que nos facilitan la vida',
                  type: 'subgraph',
                  html: ''
                },
                {
                  id: 'es-6cn-u01-p01-se03',
                  title: 'Hablamos',
                  type: 'subgraph',
                  html: ''
                }
              ]
            },
            {
              id: 'es-6cn-u02-e02',
              type: 'epigraph', //presentation/epigraph / subgraph
              title: 'Las tres funciones vitales',
              block: [
                {
                  id: 'es-6cn-u01-e02-se01',
                  title: 'Las tres funciones vitales',
                  type: 'subgraph',
                  html: ''
                },
                {
                  id: 'es-6cn-u01-e02-se02',
                  title: 'Las tres funciones vitales',
                  type: 'subgraph',
                  html: ''
                },
                {
                  id: 'es-6cn-u01-e02-se03',
                  title: 'El cuerpo humano y la nutrición',
                  type: 'subgraph',
                  html: ''
                }
              ]
            }
          ]
        }
      ]
    },
    {
      id: 'es-6m',
      title: '6.º EP Matemáticas. Savia',
      thumb: 'images/mates.jpg',
      unit: []
    },
    {
      id: 'es-6lc',
      title: '6º EP Lengua castellana. Savia',
      thumb: 'images/lengua.jpg',
      unit: []
    }
    ,
    {
      id: 'es-6sc',
      title: 'Ciencias sociales. 6.º EP. P. Savia. Comunidad de Madrid',
      thumb: 'images/soci.jpg',
      unit: []
    },
    {
      id: 'es-2sc',
      title: '2.º ESO. Física y Química',
      thumb: 'images/quimica.jpg',
      unit: []
    }

  ]
};


var htmlData = {
  'es-6cn-u01-p01-se01':
  '     <div class="sm-ui sm-subject-badge">1</div>' +
  '        <h1 class="sm-subject-title ">El cuerpo humano y la relación</h1>' +
  '        <div class="sm-cover">' +
  '          <img alt="El cuerpo humano y la relación" src="resources/sm_050103_01_es.jpg"></div>' +
  '        <div class="sm-subject-summary">' +
  '          <div class="sm-subject-summary-header">' +
  '            <span class="sm-ui sm-summary-ribbon"></span>' +
  '            <h3>¡Qué importante es…<br></h3>' +
  '            <h3><span>mantener el cuerpo ágil!<br><span></span></span></h3>' +
  '          </div>' +
  '          <div class="sm-subject-summary-content">' +
  '            <div class="sm-line">' +
  '              <div class="sm-block sm-block-1-5">' +
  '                <div class="  "><p>Nuestro cuerpo está preparado para realizar las actividades diarias. Para que se mantenga' +
  '                  ágil, es necesario cuidarlo bien, porque no es un robot al que se le cambien las piezas fácilmente. Hacer' +
  '                  ejercicio diario es una de&nbsp; las mejores recetas para estar en plena forma.<br></p></div>' +
  '              </div>' +
  '              <div class="sm-block sm-block-1-5">' +
  '                <div><h3>Tarea final</h3>' +
  '                  <p>Los robots pueden realizar tareas precisas a gran velocidad. Pero te sorprendería descubrir hasta qué' +
  '                    punto tu cuerpo es también una <em>máquina </em>veloz. Al final de la unidad, podrás comprobarlo.<br></p>' +
  '                </div>' +
  '              </div>' +
  '            </div>' +
  '          </div>' +
  '        </div>',

  'es-6cn-u01-p01-se02':
  ' <div class="sm-section-title sm-section-title">' +
  '   <div class="sm-ui sm-title-arrow">' +
  '     <div class="sm-title-unit"></div>' +
  '   </div>' +
  '   <h2>Robots que nos facilitan la vida<br></h2>' +
  ' </div>' +
  ' <div class="sm-section-content">' +
  '   <div class="sm-reading-text">' +
  '     <p>Un robot japonés capaz de conducir un coche, caminar sobre obstáculos, subir una escalera o' +
  '     abrir puertas se lleva el primer premio en un concurso de robótica.</p>' +
  '   </div>' +
  '   <div class="sm-textblock floated-right">' +
  '     <div class="sm-media">' +
  '       <div class="sm-media-content">' +
  '         <img alt="Robots que nos facilitan la vida" title="Robots que nos facilitan la vida"' +
  '                     src="resources/sm_050103_02_es.jpg">' +
  '       </div>' +
  '     </div>' +
  '   </div>' +
  '   <p>¿Te imaginas un robot que hiciera la cama o recogiera la mesa? Aunque parezca ciencia ficción,' +
  '      hoy día los ingenieros construyen robots capaces de moverse de forma muy parecida a la del ser humano. Caminan,' +
  '      sostienen objetos, identifican voces y vuelven la cabeza cuando alguien los llama. Además, sus ojos, compuestos' +
  '      por cámaras, les permiten reconocer caras y objetos.</p>' +
  '   <p>Esto hará posible que, en un futuro, asuman multitud de tareas del hogar, algo especialmente útil para el' +
  '      cuidado de ancianos o enfermos.</p>' +
  ' </div>',

  'es-6cn-u01-p01-se03':
  '  <div class="sm-section-subtitle sm-section-subtitle">' +
  '    <div class="sm-subtitle-arrow">' +
  '      <div class="sm-ui sm-subtitle-arrow-part sm-subtitle-arrow-start"></div>' +
  '      <div class="sm-subtitle-arrow-part sm-subtitle-arrow-middle">' +
  '        <h2>Hablamos</h2>' +
  '      </div>' +
  '      <div class="sm-ui sm-subtitle-arrow-part sm-subtitle-arrow-end"></div>' +
  '    </div>' +
  '  </div>' +
  '  <div class="sm-section-content">' +
  '    <div class="sm-activity">' +
  '      <div class="sm-activity-item">' +
  '        <div class="sm-activity-list">' +
  '          <span style="">1</span>' +
  '        </div>' +
  '        <div class="sm-activity-content">' +
  '          <div class="  sm-text-seromedium ">' +
  '            <p>Según el texto, ¿en qué se parecen algunos robots al ser humano? ¿En qué' +
  '              se diferencian?</p>' +
  '          </div>' +
  '          <div class="teacher-answer" style="color:#EC038A; display: none;">' +
  '            <p>Algunos robots realizan actividades semejantes a las del ser humano gracias a su función' +
  '              de relación. Los robots captan información a través de diversos dispositivos (órganos de los sentidos en' +
  '              el ser humano), procesan la información mediante procesadores (sistema nervioso) y reaccionan ante ella' +
  '              por mecanismos que les permiten moverse, avanzar, coger objetos (aparato locomotor).</p>' +
  '            <p>Los robots funcionan gracias a diversos dispositivos electrónicos y energía. Los seres humanos somos' +
  '              seres vivos que realizamos, con la intervención de órganos y aparatos coordinados entre sí, las' +
  '              funciones vitales (nutrición, relación y reproducción).</p>' +
  '          </div>' +
  '        </div>' +
  '      </div>' +
  '      <div class="sm-activity-item">' +
  '        <div class="sm-activity-list">' +
  '          <span style="">2</span>' +
  '        </div>' +
  '        <div class="sm-activity-content">' +
  '          <div class="sm-media-actions-textblock">' +
  '          </div>' +
  '          <div class="sm-textblock floated-right">' +
  '            <div class="sm-media">' +
  '              <div class="sm-media-content">' +
  '                <img alt="Un robot que ayuda a realizar las tareas del hogar"' +
  '                     title="Un robot que ayuda a realizar las tareas del hogar" src="resources/sm_050103_03_es.jpg">' +
  '              </div>' +
  '            </div>' +
  '          </div>' +
  '          <div class="  sm-text-seromedium "><p>Observa la imagen y contesta a las siguientes preguntas:<br></p></div>' +
  '          <div class="sm-text-seroregular">' +
  '            <ol style="list-style-type: lower-alpha;" start="1">' +
  '              <li>El robot está ayudando a poner la mesa. ¿Con qué función vital relacionarías la comida que van a tomar' +
  '                Juan y su familia?</li>' +
  '              <li>¿Qué órganos de los sentidos reconoces en el robot?</li>' +
  '            </ol>' +
  '          </div>' +
  '          <div class="teacher-answer" style="color:#EC038A; display: none;">' +
  '            <div class="  ">' +
  '              <ol style="list-style-type: lower-alpha;" start="1">' +
  '                <li>Función de nutrición.</li>' +
  '                <li>Vista, oído, tacto.</li>' +
  '              </ol>' +
  '            </div>' +
  '          </div>' +
  '        </div>' +
  '      </div>' +
  '      <div class="sm-activity-item">' +
  '        <div class="sm-activity-list">' +
  '          <span style="">3</span>' +
  '        </div>' +
  '        <div class="sm-activity-content">' +
  '          <div class="sm-text-seromedium"><p>Indica cuáles son las funciones vitales.<br></p></div>' +
  '          <div class="sm-text-seroregular">' +
  '            <ol style="list-style-type: upper-alpha;" start="1">' +
  '              <li>Crecer</li>' +
  '              <li>Nutrición</li>' +
  '              <li> Nacer</li>' +
  '              <li>Relación</li>' +
  '              <li> Reproducción</li>' +
  '            </ol>' +
  '          </div>' +
  '          <div class="teacher-answer" style="color:#EC038A; display: none;">' +
  '            <p>Nutrición, relación y reproducción.</p>' +
  '          </div>' +
  '        </div>' +
  '      </div>' +
  '      <div class="sm-activity-item">' +
  '        <div class="sm-activity-list">' +
  '          <span style="">4</span>' +
  '        </div>' +
  '        <div class="sm-activity-content">' +
  '          <div class="  sm-text-seromedium ">' +
  '            <p>Nombra distintas sensaciones que es capaz de sentir el ser humano.</p>' +
  '          </div>' +
  '          <div class="teacher-answer" style="color:#EC038A; display: none;">' +
  '            <p>Estímulos internos: hambre, sed, malestar, dolor, sueño, agujetas, tristeza, angustia.' +
  '              Estímulos externos: frío, calor, olores, sabores, estímulos sonoros y visuales.</p>' +
  '          </div>' +
  '        </div>' +
  '      </div>' +
  '      <div class="sm-activity-item">' +
  '        <div class="sm-activity-list">' +
  '          <span style="">5</span>' +
  '        </div>' +
  '        <div class="sm-activity-content">' +
  '          <div class="sm-text-seromedium ">' +
  '            <p><span style="color:#8bc543;">¿Y tú qué opinas? </span>&#8203;Si pudieras' +
  '              diseñar un robot, ¿qué acciones te gustaría que realizara? Explica por qué.</p></div>' +
  '          <div class="teacher-answer" style="color:#EC038A; display: none;">' +
  '            <p><strong>Respuesta libre.</strong> Robots que faciliten la vida de personas mayores o con' +
  '              discapacidades (ciegos, sordos, tetrapléjicos…), que hagan posible la rehabilitación de enfermos, que' +
  '              puedan sustituir a personas en acciones peligrosas (bomberos, mineros, exploración submarina,' +
  '              espacial...), que realicen actividades productivas repetitivas, etc.</p>' +
  '          </div>' +
  '         </div>' +
  '        </div>' +
  '     </div>' +
  '  </div>'
};