(function ($) {
  'use strict';

  $(document).ready(function () {
    showSubject(appData.subject);

    var $card = $('.card:first');
    $card.addClass('cursor-pointer');
    $card.click(function () {
      var subject =  document.querySelector('.subject')
      subject.classList.add('animated', 'fadeOutLeft');
      $(document).trigger('OnBookShow');
    }) ;

    Util.flash('#es-6cn .card-img-top');
  });

  function showSubject(list) {
    var html = '';
    $.each(list, function (index, subject) {
      html +=  '<div id="' + subject.id + '" class="card">' +
        '        <img src="' + subject.thumb + '" class="card-img-top" alt="...">' +
        '        <div class="card-body">' +
        '          <h5 class="card-title">' + subject.title + '</h5>' +
        '        </div>' +
        '      </div>'
    });
    $('.subject .list').html(html);
  }
})(jQuery);