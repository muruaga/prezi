(function ($) {
  'use strict';

  $(document).ready(function () {
    $('.login .btn').click(function () {
      var login =  document.querySelector('.login')
      login.classList.add('animated', 'fadeOutLeft');

      var cards = $(".subject .card");
      var len =  cards.length;
      cards.delay(500).each(function(i) {
        $(this).delay(20 * (len- i)).queue(function() {
          this.classList.add('animated', 'fadeInLeft');
        })
      });
    }) ;

    Util.flash('.login .btn');
  });

})(jQuery);