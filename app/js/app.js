$(document).ready(function () {

  var $btEnter = $('#BtnEnter');
  $('[btn-transition]').click(function (event) {
    var $btn = $(this);
    var $out = $($btn.attr('out-transition'));
    var $in = $($btn.attr('in-transition'));
    var dataAvAnimation = $out.attr('data-av-animation');
    $out.removeClass(dataAvAnimation);
    $out.addClass('fadeOut');
    $in.addClass();
  });

  $('.aniview').AniView();
});

window.Util = {
  clearZoom : function () {
    $('.zoomViewport').attr('style', '');
  },

  flash: function (selector) {
    setTimeout(function () {
      var element =  document.querySelector(selector);
      element.classList.add('animated', 'flash', 'slower');
      setInterval(function () {
        element.classList.toggle("flash");
      }, 7000);
    },3000);
  }
};


@@include('./js/login.js')
@@include('./js/subject.js')
@@include('./js/book.js')

