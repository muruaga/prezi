(function ($) {
  'use strict';

  var $actualLayer = null;
  var $nav = null;

  $(document).on('OnBookShow', function () {
    $actualLayer.zoomTo({root:$('.zoomContainer'), duration:600});
  });

  var resizeTimeout = null;
  $(window).on('resize', function () {
    clearTimeout(resizeTimeout);
    setTimeout(function () {
      clearTimeout(resizeTimeout);
      $actualLayer.zoomTo({root:$('.zoomContainer'), duration:600});
    }, 100);
  });

  $(document).ready(function () {
    $nav = $('.book-nav-list');

    createHTMLBook();

    $nav.find('li:first').click(function () {
      $('.subject').removeClass('animated fadeOutLeft');
    });

    $nav.find('li:not(:first)').click(function () {
      var $layer = $('#' + $(this).attr('data'));
      showLayer($layer);
    });

    $actualLayer = $('.subblock:first');
    showLayer($actualLayer);

    $('.control-button.next').click(function () {
      showLayer($actualLayer.next());
    });

    $('.control-button.prev').click(function () {
      showLayer($actualLayer.prev());
    });
  });

  $(document).on('OnChangeLayer', function (event, infoLayer) {
    showLayer(infoLayer);
  });

  function createHTMLBook() {
    var html = '';
    $.each(appData.subject[0].unit, function (index, unit) {
      $nav.append('<li class="nav-unit" data="' + index + '"><span>' + (index + 1) + '</span></li>');
      html += getHTMLUnit(unit);
    });
    $('.book-layer-view').html(html);
  }

  function getHTMLUnit(unit) {
    var html = '';
    $.each(unit.block, function (index, block) {
      html += getHTMLBlock(block);
    });
    return html;
  }

  function getHTMLBlock(block) {
    var html = '';
    if (block.subblock) {
      $.each(block.subblock, function (index, subblock) {
        $nav.append('<li id="nav-' + subblock.id + '" class="nav-subblock" data="' + subblock.id + '"><span></span></li>');
        html += getHTMLSubBlock(subblock);
      });
    }
    return html;
  }

  function getHTMLSubBlock(subblock) {
    return '<li id="' + subblock.id + '" class="subblock"><span>' + htmlData[subblock.id] + '</span></li>';
  }

  function showLayer($layer) {
    $actualLayer.removeClass('active');
    $actualLayer = $layer;
    $actualLayer.addClass('active');
    $nav.find('.nav-subblock').removeClass('active');
    $nav.find('#nav-' + $actualLayer.attr('id')).addClass('active');
    updateButtons($actualLayer);

    $actualLayer.zoomTo({root:$('.zoomContainer'), duration:600});
  }


  function ativateLayer($layer){
    if ($actualLayer) {

    }
  }

  function updateButtons($layer){
    if($layer.next().length === 0){$('.control-button.next').hide();} else{$('.control-button.next').show();}
    if($layer.prev().length === 0){$('.control-button.prev').hide();} else{$('.control-button.prev').show();}
  }




})(jQuery);